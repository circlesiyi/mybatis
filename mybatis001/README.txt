1.MyBatis学习总结(一)——MyBatis快速入门
2.MyBatis学习总结(二)——使用MyBatis对表执行CRUD操作
3.MyBatis学习总结(三)——优化MyBatis配置文件中的配置
4.MyBatis学习总结(四)——解决字段名与实体类属性名不相同的冲突(OrderMapperTest.java)
上面的测试代码演示当实体类中的属性名和表中的字段名不一致时，使用MyBatis进行查询操作时无法查询出相应的结果的问题以及针对问题采用的两种办法：
解决办法一: 通过在查询的sql语句中定义字段名的别名，让字段名的别名和实体类的属性名一致，这样就可以表的字段名和实体类的属性名一一对应上了，
这种方式是通过在sql语句中定义别名来解决字段名和属性名的映射关系的。
解决办法二: 通过<resultMap>来映射字段名和实体类属性名的一一对应关系。这种方式是使用MyBatis提供的解决方式来解决字段名和属性名的映射关系的。
5.MyBatis学习总结(五)——实现关联表查询
	MyBatis一对一关联查询总结(ClassMapperTest.java)
　　	MyBatis中使用association标签来解决一对一的关联查询，association标签可用的属性如下：
	    property:对象属性的名称
	    javaType:对象属性的类型
	    column:所对应的外键字段名称
	    select:使用另一个查询封装的结果
	MyBatis一对多关联查询总结(ClassMapperTest2.java)   
　　	MyBatis中使用collection标签来解决一对多的关联查询，ofType属性指定集合中元素的对象类型。
6.MyBatis学习总结(六)——调用存储过程（ProcedureTest）

7.MyBatis学习总结(七)——Mybatis缓存   
正如大多数持久层框架一样，MyBatis 同样提供了一级缓存和二级缓存的支持
  1.一级缓存: 基于PerpetualCache 的 HashMap本地缓存，其存储作用域为 Session，当 Session flush 或 close 之后，该Session中的所有 Cache 就将清空。
　　2. 二级缓存与一级缓存其机制相同，默认也是采用 PerpetualCache，HashMap存储，不同在于其存储作用域为 Mapper(Namespace)，并且可自定义存储源，如 Ehcache。
　　3. 对于缓存数据更新机制，当某一个作用域(一级缓存Session/二级缓存Namespaces)的进行了 C/U/D 操作后，默认该作用域下所有 select 中的缓存将被clear。
Mybatis一级缓存测试(TestOneLevelCache.java)
二级缓存补充说明(TestTwoLevelCache.java)
映射语句文件中的所有select语句将会被缓存。
映射语句文件中的所有insert，update和delete语句会刷新缓存。
缓存会使用Least Recently Used（LRU，最近最少使用的）算法来收回。
缓存会根据指定的时间间隔来刷新。
缓存会存储1024个对象
cache标签常用属性：
<cache 
eviction="FIFO"  <!--回收策略为先进先出-->
flushInterval="60000" <!--自动刷新时间60s-->
size="512" <!--最多缓存512个引用对象-->
readOnly="true"/> <!--只读-->


	    
