package com.circle.mybatis001.model;

import java.io.Serializable;

/**
 * 说明:users表所对应的实体类
 * 
 * @author 作者 E-mail:shanrenlei@126.com
 * @version 创建时间：Feb 14, 2016 2:38:48 PM
 */
public class User implements Serializable{
	private int id;
	private String name;
	private int age;

	public User() {}
	
	public User(int id, String name, int age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
}
