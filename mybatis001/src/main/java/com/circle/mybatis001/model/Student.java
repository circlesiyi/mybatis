package com.circle.mybatis001.model;

/**
 * 说明: 定义student表所对应的实体类
 * 
 * @author 作者 E-mail:shanrenlei@126.com
 * @version 创建时间：Feb 15, 2016 11:14:05 AM
 */
public class Student {
	// 定义属性，和student表中的字段对应
	private int id; // id===>s_id
	private String name; // name===>s_name

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + "]";
	}
}