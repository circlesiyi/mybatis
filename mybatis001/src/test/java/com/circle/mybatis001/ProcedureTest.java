package com.circle.mybatis001;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import com.circle.mybatis001.util.MyBatisUtil;

/**
 * 说明:测试调用存储过程
 * 
 * @author 作者 E-mail:shanrenlei@126.com
 * @version 创建时间：Feb 15, 2016 11:42:31 AM
 */
public class ProcedureTest {

	@Test
	public void testGetUserCount() {
		SqlSession sqlSession = MyBatisUtil.getSqlSession();
		/**
		 * 映射sql的标识字符串，
		 * me.gacl.mapping.userMapper是userMapper.xml文件中mapper标签的namespace属性的值，
		 * getUserCount是select标签的id属性值，通过select标签的id属性值就可以找到要执行的SQL
		 */
		String statement = "com.circle.mybatis001.mapper.userMapper.getUserCount";// 映射sql的标识字符串
		Map<String, Integer> parameterMap = new HashMap<String, Integer>();
		parameterMap.put("sexid", 1);
		parameterMap.put("usercount", -1);
		sqlSession.selectOne(statement, parameterMap);
		Integer result = parameterMap.get("usercount");
		System.out.println(result);
		sqlSession.close();
	}
}