package com.circle.service;

import java.util.List;

import com.circle.model.User;

/**
 * 说明:
 * 
 * @author 作者 E-mail:shanrenlei@126.com
 * @version 创建时间：Feb 15, 2016 4:11:06 PM
 */
public interface UserServiceI {
	/**
	 * 添加用户
	 * 
	 * @param user
	 */
	void addUser(User user);

	/**
	 * 根据用户id获取用户
	 * 
	 * @param userId
	 * @return
	 */
	User getUserById(String userId);
	
    /**获取所有用户信息
     * @return List<User>
     */
    List<User> getAllUser();
}