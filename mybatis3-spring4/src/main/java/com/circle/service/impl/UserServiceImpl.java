package com.circle.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.circle.model.User;
import com.circle.persistence.dao.UserMapper;
import com.circle.service.UserServiceI;

/**
* 说明: 
*  使用@Service注解将UserServiceImpl类标注为一个service
 * service的id是userService
* @author 作者 E-mail:shanrenlei@126.com
* @version 创建时间：Feb 15, 2016 4:12:54 PM
*/
@Service("userService")
public class UserServiceImpl implements UserServiceI {


    /**
     * 使用@Autowired注解标注userMapper变量，
     * 当需要使用UserMapper时，Spring就会自动注入UserMapper
     */
    @Autowired
    private UserMapper userMapper;//注入dao

    @Override
    public void addUser(User user) {
        userMapper.insert(user);
    }

    @Override
    public User getUserById(String userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

	@Override
	public List<User> getAllUser() {
		return userMapper.getAllUser();
	}
}
