1.新建maven项目
2.pom.xml文件中添加插件
3.在项目的/src/main/resources(默认目录)的文件目录下加入generateConfig.xml

4：项目 右键--》run as --》 maven bulid --》弹出对话框 --》在goals中输入mybatis-generator:generate 或者 点击select --》选择你的mybatis插件 --》apply --》run
运行命令:
mvn mybatis-generator:generate


说明:
UserMapper.xml这个文件的内容是编写操作t_user表的SQL语句，重点说一下UserMapper.xml配置中需要注意的几个小细节问题：
　　1、UserMapper.xml的<mapper>标签的namespace必须是UserMapper接口的全类名，既<mapper namespace="me.gacl.dao.UserMapper" >
　　2、UserMapper.xml的定义操作数据库的<select><delete><update><insert>这些标签的id属性的值必须和UserMapper接口定义的方法名一致，如下图所示：

如果是ibatis2.3.x 则使用：
<context id="DB2Tables" targetRuntime="Ibatis2Java5">
<javaClientGenerator type="SPRING"

如果使用 type=XMLMAPPER （xml直接实现dao）
需要targetRuntime is MyBatis3 而且不向下兼容
http://www.mybatis.org/generator/configreference/javaClientGenerator.html